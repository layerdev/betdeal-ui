module.exports={white:"#ffffff",black:"#000",semiGray:"#e8e8e8",gray:"#cacaca",grayLight:"#f1f1f1",grayDark:"#5f5f5f",graySemiDark:"#aeaeae",dark:"#425a70",darkTransparent:"rgba(255, 255, 255, .2)",yellow:"#e6c049",yellowLight:"#f9f9da",blue:"#007eff",green:"#04bf3c",red:"#cc0000"};
//# sourceMappingURL=themes.js.map
