# Betdeal UI
BetDeal UI React Component.


## Installation
```bash
# yarn
yarn add https://layerdev@bitbucket.org/layerdev/betdeal-ui.git

# npm
npm i --save https://layerdev@bitbucket.org/layerdev/betdeal-ui.git
```

## Usage

```javascript
// Example usage
import { Form, Button } from 'betdeal-ui/form'

export default () => (
  <Form>
    (() => {
      <Button>Button</Button>
    })
  </Form>
)
```


## License
MIT - Copyright (c) 2019 Betdeal.